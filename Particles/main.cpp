#include <windows.h>
#include <string>
#include <cassert>
#include <d3d11.h>
#include <vector>
#include <iomanip>

#include "WindowUtils.h"
#include "D3DUtil.h"
#include "D3D.h"
#include "CommonStates.h"
#include "SimpleMath.h"
#include "TexCache.h"
#include "Sprite.h"
#include "SpriteFont.h"
#include "ParticleSys.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;




class PlayMode
{
public:
	PlayMode(MyD3D& d3d)
		:mD3D(d3d), mBgnd(d3d)
	{
		mBgnd.SetTex(*d3d.GetCache().LoadTexture(&d3d.GetDevice(), "starfield.dds"));
		mBgnd.SetScale(Vector2(WinUtil::Get().GetClientWidth() / mBgnd.GetTexData().dim.x, 
						WinUtil::Get().GetClientHeight() / mBgnd.GetTexData().dim.y));
		mpFont = new SpriteFont(&d3d.GetDevice(), L"data/fonts/comicSansMS.spritefont");
		assert(mpFont);
	}
	void Update(float dTime) {
	}
	void Render(float dTime, SpriteBatch& batch) {
		mBgnd.Draw(batch);

	}
	void Release()
	{
		delete mpFont;
		mpFont = nullptr;
	}
private:
	MyD3D& mD3D;
	Sprite mBgnd;
	SpriteFont *mpFont = nullptr;

};


/*
Blank solution - setup DX11 and blank the screen
*/ 

class Game
{
public:
	enum class State { PLAY };
	State state = State::PLAY;
	
	Game(MyD3D& d3d);

	PlayMode mPMode;

	void Release();
	void Update(float dTime);
	void Render(float dTime);

	ParticleSys psys;
private:
	MyD3D& mD3D;
	SpriteBatch *pSB = nullptr;
};






Game::Game(MyD3D& d3d)
	: mPMode(d3d), mD3D(d3d), pSB(nullptr)
{
	pSB = new SpriteBatch(&mD3D.GetDeviceCtx());
	psys.Init(d3d);

	Emitter *em = psys.GetNewEmitter();
	if (em)
	{
		em->alive = true;
		em->colour1 = Vector4(0.5f, 0.f, 0.f, 1.f);
		em->colour2 = Vector4(0.5f, 0.5f, 0.f, 1.f);
		em->initSpeed = Vector2(30, 60);
		em->initVel = Vector2(0, 0);
		em->life = 3;
		em->numToEmit = 500000;
		em->numAtOnce = 30;
		em->rate = 0.0125f;
		em->scale = Vector2(0.5f, 0.5f);
		em->pos = Vector2(WinUtil::Get().GetClientWidth() / 2.f, WinUtil::Get().GetClientHeight() / 2.f);
	}
}


//any memory or resources we made need releasing at the end
void Game::Release()
{
	mPMode.Release();
	delete pSB;
	pSB = nullptr;
}

//called over and over, use it to update game logic
void Game::Update(float dTime)
{
	switch (state)
	{
	case State::PLAY:
		mPMode.Update(dTime);
	}
	psys.Update(dTime);
}

//called over and over, use it to render things
void Game::Render(float dTime)
{
	mD3D.BeginRender(Colours::Black);


	CommonStates dxstate(&mD3D.GetDevice());
	pSB->Begin(SpriteSortMode_Deferred, dxstate.NonPremultiplied(),&mD3D.GetWrapSampler());

	switch (state)
	{
	case State::PLAY:
		mPMode.Render(dTime, *pSB);
	}
	//psys.Render(*pSB, dTime);
	pSB->End();

	pSB->Begin(SpriteSortMode_Deferred, dxstate.Additive(), &mD3D.GetWrapSampler());
	psys.Render(*pSB, dTime);
	pSB->End();


	mD3D.EndRender();
}

//if ALT+ENTER or resize or drag window we might want do
//something like pause the game perhaps, but we definitely
//need to let D3D know what's happened (OnResize_Default).
void OnResize(int screenWidth, int screenHeight, MyD3D& d3d)
{
	d3d.OnResize_Default(screenWidth, screenHeight);
}

//messages come from windows all the time, should we respond to any specific ones?
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case 27:
		case 'q':
		case 'Q':
			PostQuitMessage(0);
			return 0;
		}
	}

	//default message handling (resize window, full screen, etc)
	return WinUtil::DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

//main entry point for the game
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{

	int w(512), h(512);
	//int defaults[] = { 640,480, 800,600, 1024,768, 1280,1024 };
		//WinUtil::ChooseRes(w, h, defaults, 4);
	if (!WinUtil::Get().InitMainWindow(w, h, hInstance, "Fezzy", MainWndProc, true))
		assert(false);

	MyD3D d3d;
	if (!d3d.InitDirect3D(OnResize))
		assert(false);
	WinUtil::Get().SetD3D(d3d);
	d3d.GetCache().SetAssetPath("data/");
	Game game(d3d);

	bool canUpdateRender;
	float dTime = 0;
	while (WinUtil::Get().BeginLoop(canUpdateRender))
	{
		if (canUpdateRender)
		{
			game.Update(dTime);
			game.Render(dTime);
		}
		dTime = WinUtil::Get().EndLoop(canUpdateRender);
	}

	game.Release();
	d3d.ReleaseD3D(true);	
	return 0;
}

